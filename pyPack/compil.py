#!/usr/bin/python3
#-*- coding: utf8 -*-
"""
    Génèrer le document à compiler, le compile en pdf, crée un pdf ajusté et un png ajusté
"""
pass

# Memo couleurs du terminal
# https://chamilo.univ-grenoble-alpes.fr/courses/IUT1RT1M2109/document/1718-Sokoban/build/sequences_ansi.html 


# On fait les imports nécessaires selon le contexte
# Pour générer la documentation, les répertoires, ...
import os
# Pour les expressions régulières
import re
# Pour mesurer le temps de traitement du script
from datetime import datetime 

import glob

def generateTex(file : str,source_ex : str):
    """
    Générer un fichier tex prêt à compiler
    """
    pass
    #On crée e dossier qui va accueillir les fichiers tex prêts à compiler
    if not os.path.exists("./tex_a_compiler/"):
        os.mkdir("./tex_a_compiler/")

    #On ouvre le fichier dans lequel on va écrire
    myTex = open("./tex_a_compiler/"+file+".tex","w")

    #On ouvre le préambule
    with open("./preambule/preambule.tex","r") as preambule:
        preamb_lines = preambule.read()

    #On ouvre le begin{document}
    with open("./preambule/doc_begin.tex","r") as beginDoc:
        beginDoc_lines = beginDoc.read()

    #On ouvre le fichier source avec les exos
    #with open("./exercices_corrections_tex/"+source_ex+".tex","r") as exo:
    with open("./tex/"+source_ex+".tex","r") as exo:
        exo_lines = exo.read()

    #On ouvre le \end{document}
    with open("./preambule/doc_end.tex","r") as endDoc:
        endDoc_lines = endDoc.read()    

    with myTex as mt:
        mt.write(preamb_lines[0:])
        mt.write(beginDoc_lines[0:])
        mt.write(exo_lines[0:])
        mt.write(endDoc_lines[0:])
    
    #On ferme les fichiers ouverts en écriture
    myTex.close()
    
    
def trouverLignePrecedenteContenantMot(fichier, mot, nbDeLignes):
    lignesAConserver= []
    
    with open(fichier, 'r', encoding='latin-1') as file:
    #with open(fichier, 'r', encoding='utf-8') as file:
        for ligne in file:
            lignesAConserver.append(ligne)
            # Vérifier si la ligne contient le mot recherché
            if mot in ligne:
                return [lignesAConserver[-15],lignesAConserver[-14],lignesAConserver[-13],lignesAConserver[-12],lignesAConserver[-11],lignesAConserver[-10]]
            
    return None  # Retourner None si le mot n'est pas trouvé dans le fichier


    
def compilTexToPDF(source : str):
    """
    Générer le fichier PDF à partir d'un fichier source tex
    """
    pass
    #On crée le dossier qui va accueillir les fichiers pdf
    # PLUS BESOIN ?
    if not os.path.exists("./exercices_corrections_pdf/"):
        os.mkdir("./exercices_corrections_pdf/")
    
    #On lance la commande de compilation, surement dans le répertoire courant...
    
    os.chdir("tex_a_compiler")
    os.system("sh compilTex.sh "+source)
    
    #EE Version 21 Oct 2024 : Ces trois lignes suivantes ne servent plus à rien.
    fichier = source.split('.')[0]+'.log'
    motRecherche = 'No pages of output'

    ligneAGarder = trouverLignePrecedenteContenantMot(fichier, motRecherche,15)
    #ligneAGarder = None
    
    if ligneAGarder is not None:
        print()
        print()
        print("\033[31m*************************************** La compilation LaTeX a échoué *******************************************************")
        print()
        print("\033[32m*************************************** Extrait du fichier du log qui recense un pb *****************************************")
        for ee in range(len(ligneAGarder)) :
            print(ligneAGarder[ee])
        print("\033[31m")
        print("*******************************************************************************************************************************")
        print("\033[0m")
        print("                Compilez en LaTeX le fichier \033[31mtex_a_compiler/"+source+".tex\033[0m, trouvez l'erreur,")
        print("                corrigez le fichier source (ou enrichissez le préambule) et réexécutez le programme.")
        print()
        os.system("rm *.aux")
        # os.system("rm *.ps")
        os.system("rm *.log")
        os.system("rm *.out")
    
        exit()
    else:
        print("\033[34mCompilation LaTeX réussie.\033[0m")
        os.system("rm *.aux")
        # os.system("rm *.ps")
        os.system("rm *.log")
        os.system("rm *.out")
    


    # On se remet à la racine du projet
    os.chdir("../")

def pdf2png(source : str):
    """
    Générer le fichier png ajusté et le pdf ajusté à partir d'un fichier source pdf
    """
    pass    
    #On crée le dossier qui va accueillir les fichiers png
    #if not os.path.exists("./exercices_corrections_png/"):
    #    os.mkdir("./exercices_corrections_png/")
    if not os.path.exists("./tex/png/"):
        os.mkdir("./tex/png/")
    
    #On crée le dossier qui va accueillir les fichiers pdf
    # PAS BESOIN DE CROP
    if not os.path.exists("./exercices_corrections_pdf_crop/"):
        os.mkdir("./exercices_corrections_pdf_crop/")        
    
    #On lance la commande de compilation, surement dans le répertoire courant...
    os.chdir("tex_a_compiler")
    # os.system("sh dvi2png.sh "+source)
    os.system("sh pdf2png.sh "+source)

    # On se remet à la racine du projet
    os.chdir("../")

def concat_png():
    """
    Concaténer les png si nécessaire    
    """
    pass

    #On lance la commande dans le répertoire adequat
    os.chdir("tex_a_compiler")
    filenames = os.listdir("./")
    # On trie les png
    pngfiles = []
    for filename in filenames:
        if (filename.split('.')[-1] == 'png'):
            # print('Le filename png trouvé est : ', filename)
            pngfiles.append(filename)

    # On nettoie la fin des noms des fichiers
    # on ne garde que ce qui se trouve avant le -
    cleanpngfiles = []
    for pngfile in pngfiles:
        # print('pngfile : ', pngfile)
        fichierSansTiret='-'.join(pngfile.split('-')[:-1])
        # print('pngfile bis : ', fichierSansTiret)
        if (fichierSansTiret!='') : cleanpngfiles.append(fichierSansTiret)
        # if (fichierSansTiret != '') : cleanpngfiles.append(pngfile)
        # else : 
        # cleanpngfiles.append(fichierSansTiret)
    
    # On compte les occurences différentes et les effectifs 
    countpngfiles = []
    # print('Fichiers dans cleanpngfiles: ', cleanpngfiles)
    
    for cleanpngfile in cleanpngfiles:
        # print('Compteur 1 : ', cleanpngfile)
        countpngfiles.append([cleanpngfile,cleanpngfiles.count(cleanpngfile)])

    # On supprime les doublons
    newcountpngfiles = []
    for countpngfile in countpngfiles:
        # print('Compteur 2 : ', countpngfile)
        if (countpngfile not in newcountpngfiles):
            # print('Fichier ajouté : xx', countpngfile[0],'aaaa',len(countpngfile[0]))
            if (countpngfile[0] != '') : newcountpngfiles.append(countpngfile)
    
    # On concatène ce qu'il faut
    # On crée la chaine du premier argument    
    for i in range(len(newcountpngfiles)):
        # print('Compteur de fichier png trouvé : ', i,newcountpngfiles[i][1])
        firstArgStr = ''
        for j in range(int(newcountpngfiles[i][1])):
            # print('Compteur de fichier png 2 trouvé : ', j)
            firstArgStr+=newcountpngfiles[i][0]+'-'+str(j+1)+'.png '
            # print('On crée ',firstArgStr)
        # On fait la concaténétion
        # print('On crée aussi ',firstArgStr+newcountpngfiles[i][0]+".png")        
        os.system("sh concat_png.sh \""+firstArgStr+"\" "+newcountpngfiles[i][0]+".png")
    
    # print('On crée ',firstArgStr+newcountpngfiles[len(newcountpngfiles)-1][0]+".png")        
        
    # On supprime les fichiers png qui contiennent un -
    filenames = os.listdir("./")    
    # On trie les png    
    for filename in filenames:
        if (filename.split('.')[-1] == 'png' and '-' in filename):
            # print ('On supprime ',filename)
            os.system("rm "+filename)
    
    # On se remet à la racine du projet
    os.chdir("../")

def copyAllFiles(source : str):
    """
    Copier les fichiers générés là où il faut.
    """
    pass
    # On copie le fichier pdf    
    os.system("sh copy.sh ./tex_a_compiler/"+source+".pdf ./exercices_corrections_pdf/"+source+".pdf")
    
    # On copie le fichier png
    # os.system("cp ./tex_a_compiler/*.png ./exercices_corrections_png/" )
    os.system("cp ./tex_a_compiler/*.png ./tex/png/" )

    # On copie le fichier pdf ajusté
    # Plus besoin ?
    # os.system("cp ./tex_a_compiler/"+source+"-crop.pdf ./exercices_corrections_pdf_crop/"+source+"-crop.pdf" )

    # On crée le dossier qui va accueillir les fichiers tex autonomes
    if not os.path.exists("./exercices_corrections_tex_autonome/"):
        os.mkdir("./exercices_corrections_tex_autonome/")
    # On copie le fichier tex
    os.system("cp ./tex_a_compiler/"+source+".tex ./exercices_corrections_tex_autonome/"+source+".tex" )

    # On crée le dossier qui va accueillir les fichiers eps
    # Plus besoin ?
    # if not os.path.exists("./exercices_corrections_eps/"):
    #    os.mkdir("./exercices_corrections_eps/")
    # On copie le fichier eps
    # On teste si des fichiers images existent avant de les copier
    # nom=glob.glob('*.eps')
    
    # if len(nom)!=0 :
    #   os.system("cp ./tex_a_compiler/*.eps ./exercices_corrections_eps/" )

def cleanPath(path):
    """
    Nettoyer les fichiers de compilation inutiles
    """
    pass
    # os.chdir("tex_a_compiler")
    os.chdir(path)
    os.system("rm *.aux")
    os.system("rm *.dvi")
    # os.system("rm *.log")
    os.system("rm *.out")
    os.system("rm *.png")
    os.system("rm *.ps")
    os.system("rm *.tex")
    os.system("rm *.pdf")
    # On teste si des fichiers images existent avant de les supprimer
    nom=glob.glob('*.eps')
    if len(nom)!=0 :
        os.system("rm *.eps")
    # On se remet à la racine du projet
    os.chdir("../")
    os.system("rm -rf exercices_corrections_*")
    

def cutTex2Ex(file : str,source : str)->list:
    """
    Découpe un fichier source *.tex
    
    file : le début du nom du fichier dans lequel on va écrire
    source : le fichier source contenant les exos
    """
    pass
    # Est-ce un corrigé ?
    if ("corrige" in source.lower() or "correction" in source.lower()):
        plus = '_cor'
    else:
        plus = ''
        
    technologique = ("sti2d" in source.lower()) # Pour prendre en compte la spécificité de nommage des filières technologiques
    
    # On copie les fichiers images eps s'il y en a
    # On teste si des fichiers images existent avant de les copier
    nom=glob.glob('./sujets_corrections_tex/*.eps')
    
    if len(nom)!=0 :
        os.system("cp ./sujets_corrections_tex/*.eps ./tex_a_compiler/" )
    
    # On ouvre le fichier source
    with open("./sujets_corrections_tex/"+source+".tex","r") as source:
        source_lines = source.readlines()

    # Un tableau pour les indices contenant le début des exos
    indices = []

    # Un tableau avec les textes à chercher dans le code pour repérer les exercices
    strExs = [
        "textbf{Exercice",
        "textbf{\\textsc{Exercice",
        "textbf{EXERCICE",
        "textbf{\\large EXERCICE",
        "textbf{\\large\\textsc{Exercice",
        "textbf{\\Large\\textsc{Exercice",
        "textbf{\\Large{\\textsc{Exercice",
        "textbf{\\large Exercice",
        "textbf{\\large{}Exercice",
        "textbf{{\\large \\textsc{Exercice",
        "textbf{\\large \\textsc{Exercice",
        "\\section{Exercice",
        "\\section*{Exercice",
        "{\\Large\\bf{}Exercice"
    ]
    
    # Un tableau pour le nommage spécifique des filières technologiques
    techno = []
    indicetechno=1

    for i in range(len(source_lines)):
        # Selon les années la commande pour le style des exos n'est pas la même        
        for strEx in strExs:
            if (strEx in source_lines[i]):
                indices.append(i)
                # print(i,source_lines[i])
                if ("pcm" in source_lines[i].lower()) : techno.append('pcm')
                elif ("math" in source_lines[i].lower()) : techno.append('maths')
                else : 
                    techno.append('mq'+str(indicetechno))
                    indicetechno = indicetechno+1
                        
    indices.append(len(source_lines))
    
    if len(indices)==1 :
        print("\033[31m*******************************************************************************************************************************")
        print()
        print("                                                    FICHIER NON DECOUPE")
        print()
        print()
        print("                 Le sujet \033[32m"+source.name+"\033[31m n'a pas pu être découpé.")
        print("                  Pour découper le sujet, le programme n'a pas réussi à trouver le début de chaque exercice")
        print("                                             dans le code source du sujet.")
        print("\033[33m")
        print("  Il va falloir regarder dans le fichier \033[32m"+source.name+"\033[33m comment")
        print("  est introduit en LaTeX chaque exercice et rajouter cette introduction dans \033[32mcompil.py\033[33m vers la ligne 300. \033[31mAu boulot !")
        print()
        print("*******************************************************************************************************************************")
        print("\033[0m")
        print("Après avoir effectué cette modification, relancer le programme.")
        exit()
        
    # On commence par voir s'il y a des annexes
    # S'il y a des annexes on invite à modifier les sources du sujets dans un message d'erreur
    # On vérifie s'il y a ou non le mot annexe dans une ligne du dernier découpage
    # Plutôt dans tout le document Oui c'est mieux !
    isAnnexe = False

    for i in range(indices[0],indices[len(indices)-1]):
        if ("annexe" in source_lines[i].lower()):
            isAnnexe = True
             # print (i, source_lines[i])    

    if (isAnnexe):
        print()
        print("\033[31m*******************************************************************************************************************************")
        print()
        print("                                         Traitement des annexes")
        print()
        print()
        print("                 Le sujet \033[32m"+source.name+"\033[31m contient des annexes.")
        print("             Pour découper le sujet, il faut, au préalable, replacer manuellement l'annexe au niveau des bons exercices")
        print("                               dans le code source du sujet avant de relancer le traitement.")
        print("\033[33m")
        print("            Il ne faut pas oublier, non plus, de supprimer les références aux exercices dans les annexes en même temps ")
        print("                                           car cela conditionne le découpage des exercices.")
        print("\033[31m")
        print("*******************************************************************************************************************************")
        print("\033[0m")
        # e=input("Après avoir effectué cette modification, relancer le programme. Taper Entrée pour poursuivre.")
        exit()
        
    else: # else inutile
        #On crée le dossier qui va accueillir les fichiers tex 
        #if not os.path.exists("./exercices_corrections_tex/"):
        #    os.mkdir("./exercices_corrections_tex/")
        if not os.path.exists("./tex/"):
            os.mkdir("./tex/")
        
        # On génére tous les fichiers
        for i in range(len(indices)-1):
            # On ouvre le fichier dans lequel on va écrire
            # EE : On traite le cas des fichiers de filières techno
            if (technologique) : myTex = open("./tex/"+file+"_"+techno[i]+plus+".tex","w")  
            else : myTex = open("./tex/"+file+"_"+str(i+1)+plus+".tex","w")       
            # On ajoute les lignes
            # for j in range(indices[i],indices[i+1]):
            for j in range(indices[i]+1,indices[i+1]):
                # On ajoute les lignes sauf \end{document} ou \newpage
                # if ("bf{}Exercice" not in source_lines[j] and "{Exercice" not in source_lines[j] and "\\end{document}" not in source_lines[j] and "\\newpage" not in source_lines[j] and "textbf{EXERCICE" not in source_lines[j] and "textbf{Exercice" not in source_lines[j] and "textbf{\\large{}Exercice" not in source_lines[j] and "textbf{\\large Exercice" not in source_lines[j] and "textbf{\\large EXERCICE" not in source_lines[j] and "textbf{\\textsc{Exercice" not in source_lines[j] and "textbf{{\\large \\textsc{Exercice" not in source_lines[j] and "Maîtrise de la langue : 4 points" not in source_lines[j]):
                if ("\\end{document}" not in source_lines[j] and "\\newpage" not in source_lines[j]):
                    myTex.writelines(source_lines[j])
            myTex.close()
            
    # exit()
            
def generateFiles(file : str,source_ex : str):
    """
    Générer tous les fichiers *.pdf *.pdf ajusté *.png *.tex prêt à compiler 
    """
    pass
    # On génère le fichier tex à compiler
    print()
    print('111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111')
    print("            Etape 1 : On tente de générer ce fichier tex prêt à compiler : "+source_ex)
    print('111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111')
    print()
    generateTex(file,source_ex)

    
    
    #On compile
    print()
    print('222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222')
    print("             Etape 2 : On tente de compiler le fichier tex généré ci-dessus : "+source_ex)
    # print("                     Le fichier de log qui va s'afficher ci-dessous va être très verbeux.")
    print('222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222')
    print()
    compilTexToPDF(file)
    
    
    #On convertit en png
    print()
    print('333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333')
    print("          Etape 3 : On tente de convertir le fichier "+source_ex+".pdf en : ")
    print("                                      -  un fichier pdf ajusté")
    print("                                      -  et diverses images png")
    print()
    pdf2png(file)
    print('333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333')
    # exit()
    
    # On concatène les png à concaténer
    print()
    print('444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444')
    print("      Etape 4 : On concatène les images obtenues en une seule image png : "+source_ex+".png")
    print("                         Si à l'étape d'avant, un seul fichier png a été généré,  ")
    print("                     un message d'erreur peut être généré mais sans effet, ni importance.")
    print()
    concat_png()
    print('444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444')
        
    #On copie les fichiers pdf, png, pdf-crop dans les bons dossiers
    print()
    print('555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555')
    print("      Etape 5 : Si tout s'est bien passé, on copie : ")
    print("         - "+source_ex+".pdf dans 'exercices_corrections_pdf/'")
    #print("         - "+source_ex+".png dans 'exercices_corrections_png/'")
    print("         - "+source_ex+".png dans 'tex/png/'")
    # print("         - "+source_ex+"-crop.pdf (PDF ajusté) dans 'exercices_corrections_pdf_crop/'")
    print("         - "+source_ex+".tex dans 'exercices_corrections__tex_autonome/'")
    print("         - tous les fichiers images dans 'exercices_corrections__eps/'")
    copyAllFiles(file)   
    print('555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555')
    # exit()
    
def generateFileName(source_ex : str)->str:
    """
    Générer le nom du fichier pour le découpage au format dnb_aaaa_mm_lieu
    """
    pass
    aaaa = ''
    mm = ''
    lieu = ''   
    
    # On passe le nom du fichier en minuscules
    cleanSource = source_ex.lower()
    # On supprime les underscores et les -
    cleanSource = re.sub('[_-]', '', cleanSource)
    
    # Possibles pour les années
    annees = []
    for i in range(2001,datetime.now().year+1):
        annees.append(i)    
    # On récupère l'année
    for annee in annees:
        if (str(annee) in source_ex):
            aaaa = str(annee)
    # Possibles pour les mois
    months = [['jan','01'],['fev','02'],['mars','03'],['avril','04'],['mai','05'],['juin','06'],['_06_','06'],['juillet','07'],['aout','08'],['sept','09'],['oct','10'],['nov','11'],['dec','12']]
    # On récupère le mois
    for month in months:
        # Créer un motif pour vérifier '02' en tant que mot entier
        # pattern = r'(?<!\d)' + month[1] + r'(?!\d)'
        pattern = r'(?<!sujet)(?<!\d)' + month[1] + r'(?!\d)'
        if ((month[0] in source_ex.lower()) or (re.search(pattern, source_ex.lower()))) :
            mm = month[1] 
            break
    if (mm == ''):
        if ('e3c' in cleanSource):
            mm='00'
        else:
            mm = 'bugMois'    
            print("\033[31m************************************************** PROBLEME ***************************************************************************************")
            print()
            print("     \033[32mAucun mois dans le nom du fichier \033[31m"+source_ex+"\033[32m.")
            print()
            print("     Choisissez parmi la liste ci-dessous, corrigez le nom du fichier et relancez le programme.")
            print()
            print("     Mois possibles :  jan, fev, mars, avril, mai, juin, juillet, aout, sept, oct, nov, dec")
            print()
            print("\033[31m***************************************************************************************************************************************************\033[0m")
            exit()

    # Possibles pour les centres
    centres = [
        'ameriquesud',
        'amsud',
        'amdusud',
        'amdusudsecours',
        'ameriquenord',
        'ameriquedunord',
        'amdunord',
        'amerinord',
        'antillesguyane',
        'antilles',
        'asie',        
        'caledonie',
        'etrangers',
        'etranger',
        'maroc',  
        'mexique',      
        'metropole',
        'polynesie',
        'pondichery',
        'reunion',
        'wallis',
        'grece',                
        'madagascar',
        'suede'
    ]
    if ('bac' in cleanSource):
        centres.append('sujet0')
       # centres.append('sujet1') # pour le bac d'avant 2022
       # centres.append('sujet2') # pour le bac d'avant 2022
    
    elif ('sti2d' in cleanSource):
        centres.append('sujet01')
        centres.append('sujet02')
        
    elif ('e3c' in cleanSource):
        centres.append('specimen1')
        centres.append('specimen2')
        centres.append('specimen3')
        centres.append('specimen4')
        # Utilisation d'une boucle for pour ajouter les éléments "sujet1" à "sujet70"
        for i in range(1, 71):
            centres.append(f"sujet{i}")
    
    elif ('crpe' in cleanSource):
        # Utilisation d'une boucle for pour ajouter les éléments "g1" à "g9"
        for i in range(1, 10):
            centres.append(f"g{i}")
    
    # On passe le nom du fichier en minuscules
    # cleanSource = source_ex.lower()
    # On supprime les underscores et les -
    # cleanSource = re.sub('[_-]', '', cleanSource)
    # On récupère le lieu
    for centre in centres:
        if (centre in cleanSource):
            if centre == 'amdunord' or centre == 'ameriquedunord' or centre == 'amerinord':
                lieu = 'ameriquenord'
            elif centre == 'amdusud' or centre == 'amdusudsecours':
                lieu = 'ameriquesud'                
            elif centre == 'wallis':
                lieu = 'wallisfutuna'
            elif (centre == 'antilles' or centre =='antillesguyane'):
                #lieu = 'antillesguyane'
                lieu = 'metropole'
            elif (centre =='etranger' or centre =='etrangers'):
                if ('gpe1' in cleanSource):
                    lieu = 'etrangers_groupe1'
                elif ('gpe2' in cleanSource):
                    lieu = 'etrangers_groupe2'
                else :
                    lieu = 'etrangers'
            elif (centre == 'maroc'):
                lieu = 'etrangers_maroc'
            elif re.fullmatch(r'g\d+', centre):
                nom = re.fullmatch(r'g\d+', centre)
                lieu = 'groupement '+nom.group(1)
            elif (centre == 'sujet01'):
                lieu = 'sujet0V1'
            elif (centre == 'sujet02'):
                lieu = 'sujet0V2'
            else:
                lieu = centre
    
    if ('devoile' in cleanSource):
        lieu = lieu + '_devoile'
            
    if (lieu == ''):
        # lieu = 'bugLieu'
        print("\033[31m*****************************************************************************************************************************************")
        print()
        print("     Aucune centre correct dans le nom du fichier \033[32m"+source_ex+"\033[31m.")
        print()
        print("     Choisissez parmi la liste ci-dessous, corrigez le nom du fichier et relancez le programme.")
        print()
        print("     Centres possibles :  amdunord, amdusud, antilles, asie, caledonie, etrangers, grece, madagascar, maroc, metropole, polynesie, pondichery, wallis")
        # print("     Centres possibles spécifiques pour le bac : sujet0, sujet1, sujet2")
        print("     Centres possibles spécifiques pour E3C : specimen1, specimen2, specimen3, specimen4")
        print()
        print("*****************************************************************************************************************************************\033[0m")
        exit()
        
    # On forme le nom du fichier
    if ('brevet' in cleanSource):
        if 'mathalea' in cleanSource:
            filename = "dnb_"+aaaa+"_"+mm+"_"+lieu+"_mathalea"
        else:
            filename = "dnb_"+aaaa+"_"+mm+"_"+lieu
    elif ('dnb' in cleanSource):
            filename = "dnb_"+aaaa+"_"+mm+"_"+lieu
    elif ('bac' in cleanSource or 'spe' in cleanSource):
        if ((not('sujet0' in cleanSource)) and (not('sujet1' in cleanSource)) and  (not('sujet2' in cleanSource)) and (not('j1' in cleanSource)) and  (not('j2' in cleanSource))) :
            print("\033[31m*****************************************************************************************************************************************")
            print()
            print("      Aucune version dans le nom du fichier \033[32m"+source_ex+"\033[31m : ni sujet1, ni sujet2. Corrigez et relancez le programme.")
            print()
            print("*****************************************************************************************************************************************\033[0m")
            exit()
        elif ('sujet1' in cleanSource or 'j1' in cleanSource) :
            filename = "bac_"+aaaa+"_"+mm+"_"+"sujet1_"+lieu
        elif ('sujet2' in cleanSource or 'j2' in cleanSource) :
            filename = "bac_"+aaaa+"_"+mm+"_"+"sujet2_"+lieu
        else :
             filename = "bac_"+aaaa+"_"+mm+"_"+lieu
    # elif ('e3c' in cleanSource or 'j2' in cleanSource):
    elif ('e3c' in cleanSource):
        filename = "e3c_"+aaaa+"_"+mm+"_"+lieu
    elif ('sti2d' in cleanSource):
        if ('sujet1' in cleanSource or 'j1' in cleanSource)  :
            filename = "sti2d_"+aaaa+"_"+mm+"_"+lieu+"_j1"
        elif ('sujet2' in cleanSource or 'j2' in cleanSource)  :
            filename = "sti2d_"+aaaa+"_"+mm+"_"+lieu+"_j2"
        else :
            filename = "sti2d_"+aaaa+"_"+mm+"_"+lieu
    elif ('stl' in cleanSource):
        filename = "stl_"+aaaa+"_"+mm+"_"+lieu
    else :
        print("\033[31m*****************************************************************************************************************************************")
        print()
        print("                      Aucun diplôme dans le nom du fichier \033[32m"+source_ex+"\033[31m : .")
        print("                                      ni dnb, ni bac, ni e3c, ni sti2d, ni stl.")
        print("                                         Corrigez et relancez le programme.")
        print()
        print("*****************************************************************************************************************************************\033[0m")
        exit()
    
    
    # print(filename)
    # exit()
    return filename

if __name__ == "__main__":    
    os.system("clear")
    # On récupère la date au début du traitement
    start_time = datetime.now()

    # On évalue le temps de traitement
    end_time = datetime.now()
    print("=============================================================================")
    print("  Durée de traitement : ",end_time-start_time)        
    print("=============================================================================")

