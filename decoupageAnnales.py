#!/usr/bin/python3
#-*- coding: utf8 -*-
"""
Découper un sujet.

Génèrer le document à compiler, la documentation, les versions pdf, pdf ajustée et png
"""

__author__      = "Sébastien Lozano (modifié par Eric Elter)"
__licence__   = "MIT"

pass

# On fait les imports nécessaires selon le contexte
# Pour générer la documentation
import os
# Pour mesurer le temps de traitement du script
from datetime import datetime 

# Pour générer tex, pdf,png
import pyPack.compil as compil
import pyPack.html as html

# Pour tester s'il manque des fichiers
def isAnyBug():
    """
    Pour tester s'il y a eu des bugs de compilation.
    On compare les fichiers du dossier **exercices_corrections_png** et du  dossier **exercices_corrections_tex**
    """
    pass

    # if (not os.path.exists("./exercices_corrections_png/")) or (not os.path.exists("./exercices_corrections_tex/")):
    if (not os.path.exists("./tex/")) or (not os.path.exists("./tex/png/")):
        print("Les dossiers pour les png et/ou les tex n'existent pas")
    else:
        #On lance la commande dans le répertoire adequat
        #os.chdir("exercices_corrections_png")
        os.chdir("tex/png")
        filenames = os.listdir("./")
        filenames.sort()       
        # On trie les png
        pngfiles = []
        for filename in filenames:
            if (filename.split('.')[-1] == 'png'):
                pngfiles.append(filename.split('.')[0])
                print("Exercice image créé :",filename)
        #print(pngfiles)
        print("nombre pngfiles : ",len(pngfiles))
        print()
        
        #On lance la commande dans le répertoire adequat
        #os.chdir("../exercices_corrections_tex")
        os.chdir("..")
        filenames = os.listdir("./")        
        filenames.sort()       
        # On trie les tex
        texfiles = []
        for filename in filenames:
            if (filename.split('.')[-1] == 'tex'):
                texfiles.append(filename.split('.')[0])
                print("Exercice tex créé :",filename)
        #print(texfiles)
        print("nombre texfiles : ",len(texfiles))
        print()
        
        #On teste déjà les cardinaux
        if (len(pngfiles) != len(texfiles)):
            print("BUG")
            bugfiles = []
            for texfile in texfiles:
                if (texfile not in pngfiles):
                    bugfiles.append(texfile)
            print("Fichiers problématiques : ")
            for bugfile in bugfiles:
                print(bugfile)
        else:
            cleanPath()            

def cleanPath():
    """
    Nettoyer les fichiers de compilation inutiles
    """
    pass
    os.chdir("../tex_a_compiler")
    os.system("find . -type f ! -name '*.sh' -delete")
    # os.chdir(path)
    # os.system("rm *.aux")
    # os.system("rm *.dvi")
    # os.system("rm *.log")
    # os.system("rm *.out")
    # os.system("rm *.png")
    # os.system("rm *.ps")
    # os.system("rm *.tex")
    # os.system("rm *.pdf")
    # On teste si des fichiers images existent avant de les supprimer
    # nom=glob.glob('*.eps')
    # if len(nom)!=0 :
    # os.system("rm *.eps")
    # On se remet à la racine du projet
    os.chdir("../")
    os.system("rm -rf exercices_corrections_*")
  
def decoupageEnPlusieursFichers():
    # On découpe le sujet en exos
    # Pour chaque exos
    print()
    print("*******************************************************************************************************************************")
    print()
    print("              DECOUPAGE DES FICHIERS SOURCES D'ANNALES ET CREATION DES FICHIERS TEX, PDF, PNG DE CHAQUE EXERCICE.")    
    print("                                                        C'EST PARTI....")
    print()
    print("*******************************************************************************************************************************")
    print()
    # On découpe les sujets présents dansle dossiers sujets_corrections_tex
    
    print("                                                          ***")
    print("                                                         ****")
    print("                                                        *****")
    print("                                                       ******")
    print("                                                      *** ***")
    print("                                                     ***  ***")
    print("                                                    ***   ***")
    print("                                                   ***    ***")
    print("                                                  ***     ***")
    print("                                                          ***")
    print("                                                          ***")
    print("                                                          ***")
    print("                                                          ***")
    print("                                                          ***")
    print("                                                          ***")
    print("                                                          ***")
    print("                                                          ***")
    print("                                                        *******")
    print("                                                        *******")
        
    for (dirpath, dirnames, filenames) in os.walk('./sujets_corrections_tex/'):
        for source in filenames:
            
            #On ne traite que les sources pas les images ou le fichier .gitkeep
            if (source.split('.')[-1] != 'eps' and source.split('.')[-1] != 'jpg' and source.split('.')[-1] != 'txt' and source != ".DS_Store"):
                # On supprime l'extension du fichier
                source = '.'.join(source.split('.')[:-1])            
                # On formatte le nom du fichier de sortie
                # print('source ',source)
                source_format = compil.generateFileName(source)
                # if (source_format=='bug'):
                #    print()
                #    exit()
            
                # On découpe
                print()
                print("                         On va découper le fichier ", source_format," en une suite de fichiers.") 
                print()
                print()
                compil.cutTex2Ex(source_format,source)
    # exit()
    
import re

def get_penultimate_number(filename):
    # Recherche tous les nombres dans la chaîne
    matches = re.findall(r'\d+', filename)
    return matches[-2] if len(matches) >= 2 else None  # Retourne l'avant-dernier nombre ou None

def get_last_number(filename):
    # Recherche le dernier nombre dans la chaîne
    match = re.search(r'(\d+)(?!.*\d)', filename)
    return match.group(0) if match else None

 
# Script principal
def main():
    # On récupère la date au début du traitement
    start_time = datetime.now()

    # On génère la documentation
    # print("=============================================================================")
    # print("  Création de la documentation en cours ...  ")    
    # print(" ")
    # os.system('sh ./generateMyDoc.sh')   

    """
    choixTransfo = input(""Est-ce simplement une transformation de fichiers découpés en PNG ?
        ---> Oui : Mettre tous les fichiers dans le répertoire tex/, les nommer en minuscules (du genre crpe_l3_2025_sujet0_1_cor.tex) et taper 'O' ou 'o' pour continuer.
        ---> Non : Pour FlashBac, mettre tous les fichiers déjà découpés dans le répertoire tex/ et taper 'F' ou 'f' pour continuer.
        ---> Non : Mettre tous les fichiers découpés dans le répertoire sujets_corrections_tex/ (sauf pour flashbac, dans le répertoire tex/) et taper ce que vous voulez pour continuer.
        Taper une touche pour continuer. --> "")
    """
    choixTransfo = 'e' ################################################################################################################################################################""
    if (choixTransfo == 'f' or choixTransfo == 'F') : # Code pour FlashBac
        os.chdir("tex")
        # Vérifie si le répertoire png existe et on le supprime éventuellement
        if os.path.exists("png") and os.path.isdir("png"):
            # Parcourt tous les fichiers et sous-répertoires dans le répertoire
            for root, dirs, files in os.walk("png", topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))  # Supprime les fichiers
                for name in dirs:
                    os.rmdir(os.path.join(root, name))  # Supprime les sous-répertoires
            os.rmdir("png")  # Supprime le répertoire lui-même

        for (dirpath, dirnames, filenames) in os.walk('./'): #Renommage du fichier pour éviter les fautes de frappe
            for source in filenames:
                new_name = 'QCM_' + compil.generateFileName(source) 
                if ('cor' in source) :
                    new_name += '_cor'
                new_name += '_Ex' + get_penultimate_number(source) + '_Q' + get_last_number(source) + '.tex'
                os.rename(os.path.join(dirpath, source), os.path.join(dirpath, new_name))
        os.chdir("..")
    elif (choixTransfo != 'o' and choixTransfo != 'o') : decoupageEnPlusieursFichers()
    else :
        os.system("cp ./tex/*.eps ./tex_a_compiler/" )
    print()       
    print("                           Le nom des sources est correct et le découpe des noms de fichiers s'est bien passé.")       
    print()       
    print("*******************************************************************************************************************************")
    print()       
    print("                                                          ******")
    print("                                                        ***   ***")
    print("                                                       ***     ***")
    print("                                                      ***      ***")
    print("                                                     ***        ***")
    print("                                                     ***        ***")
    print("                                                               ***")
    print("                                                             ***")
    print("                                                           ***")
    print("                                                         ***")
    print("                                                       ***")
    print("                                                     ***")
    print("                                                     **************")
    print("                                                     **************")
    print()
    print("                                 On peut commencer à créer chaque exercice l'un après l'autre.")  
    # Tous les fichiers du répertoire /exercices_corrections_tex
    # for (dirpath, dirnames, filenames) in os.walk('./exercices_corrections_tex/'):
    for (dirpath, dirnames, filenames) in os.walk('./tex/'):
        filenames.sort()
        for source in filenames:            
    # On supprime l'extension du fichier
            if source.split('.')[1]=='tex' :
                source2 = '.'.join(source.split('.')[:-1])                        
                print()       
                print()
                print()
                print()
                print()
                print()
                print()
                print()
                print()
                print("         TACHES CI-DESSOUS : CREER L'EXERCICE "+source2+" ISSU DE LA DECOUPE PRECEDENTE. ")
                print()
                print()
                print()
                print()
                print()
                compil.generateFiles(source2,source2)
                # e=input('Vérifie si bug et continue.')
        print()
        print()
        print()
        print("         ICI S'ACHEVE LA CREATION DE TOUS LES EXERCICES ISSUS DE "+source)
        print()
        print()
            
    # On nettoie le dossier tex_a_compiler
   # compil.cleanPath("tex_a_compiler")


    print()
    print()
    print()
    print("   *********        ************     ***                    ******           ******       ***")
    print("   ***    ***       ************     ***                    ******           *******      ***")
    print("   ***     ***           ***         ***                   ***  ***          *** ****     ***")
    print("   ***      ***          ***         ***                   ***  ***          *** ****     ***")
    print("   ***     ***           ***         ***                  ***    ***         ***  ****    ***")
    print("   ***    ***            ***         ***                  ***    ***         ***  ****    ***")
    print("   *********             ***         ***                 ***      ***        ***   ****   ***")
    print("   *********             ***         ***                 ************        ***   ****   ***")
    print("   ***    ***            ***         ***                **************       ***    ****  ***")
    print("   ***     ***           ***         ***                ***        ***       ***    ****  ***")
    print("   ***      ***          ***         ***               ***          ***      ***     **** ***")
    print("   ***     ***           ***         ***               ***          ***      ***     **** ***")
    print("   ***    ***       ************     ************     ***            ***     ***      *******")
    print("   *********        ************     ************     ***            ***     ***      *******")
    print()
    print()
    print()
    

    # On génére page d'accueil
    # print("=============================================================================")
    # print("  Création de la page d'accueil en cours ...  ")    
    # html.main()

    # On évalue le temps de traitement
    end_time = datetime.now()
    print()
    print("  Durée de traitement : ",end_time-start_time)        
    print()
    
if __name__ == "__main__":
    os.system("clear")
    main()
    #os.system("clear")
    isAnyBug()
